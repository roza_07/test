package com.example.Test.dto;

import com.example.Test.enums.Role;
import com.sun.istack.NotNull;
import lombok.Data;

@Data
public class UpdateUserDto {
    @NotNull
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private Role role;
}
