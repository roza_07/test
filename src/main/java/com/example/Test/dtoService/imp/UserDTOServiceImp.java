package com.example.Test.dtoService.imp;

import com.example.Test.dto.CreateUserDto;
import com.example.Test.dto.UpdateUserDto;
import com.example.Test.dto.UserDTO;
import com.example.Test.dtoService.UserDTOService;
import com.example.Test.enums.Role;
import com.example.Test.entity.User;
import com.example.Test.exception.UserSaveException;
import com.example.Test.mapper.UserMapper;
import com.example.Test.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class UserDTOServiceImp implements UserDTOService {

    private final UserMapper userMapper;
    private final UserService userService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserDTOServiceImp(UserMapper userMapper, UserService userService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userMapper = userMapper;
        this.userService = userService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public UserDTO create(CreateUserDto createUserDto) throws UserSaveException {
        User user = new User();

        user.setFirstName(createUserDto.getFirstName());
        user.setLastName(createUserDto.getLastName());
        user.setEmail(createUserDto.getEmail());
        user.setPassword(bCryptPasswordEncoder.encode(createUserDto.getPassword()));
        user.setUsername((generateUsername(createUserDto.getFirstName()) + generateUsername(createUserDto.getLastName()) + dateFormat()).toLowerCase());
        user.setRole(checkRole(createUserDto.getRole()));

        User savedUser = userService.save(user);

        return userMapper.toUserDTO(savedUser);
    }

    @Override
    public UserDTO getById(Long id) {
        return userMapper.toUserDTO(userService.getById(id));
    }

    @Override
    public List<UserDTO> getAll() {
        return userMapper.toUserDTOList(userService.findAll());
    }

    @Override
    public UserDTO update(UpdateUserDto updateUserDto) {
        User user = userService.getById(updateUserDto.getId());

        if (updateUserDto.getFirstName() != null && updateUserDto.getLastName() != null && updateUserDto.getEmail() != null && updateUserDto.getRole() != null) {
            user.setFirstName(updateUserDto.getFirstName());
            user.setLastName(updateUserDto.getLastName());
            user.setEmail(updateUserDto.getEmail());
            user.setRole(updateUserDto.getRole());
        }
        return userMapper.toUserDTO(userService.save(user));
    }

    @Override
    public void delete(Long id) {
        userService.deleteById(id);
    }

    private String generateUsername(String username) {
        if (username.length() > 3)
            return username.substring(0, 3);
        else
            return username;
    }

    private String dateFormat() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
        return simpleDateFormat.format(new Date());
    }

    private Role checkRole(Role role) throws UserSaveException {
        if (Role.ADMIN == role)
            throw new UserSaveException("You can't create an " + role);
        return role;
    }
}